FROM ubuntu:xenial
MAINTAINER Masashi Suguro<ecologydriver@gmail.com>

RUN apt-get update && apt-get install -y software-properties-common
RUN apt-get install -y texlive-lang-cjk texlive-fonts-recommended texlive-fonts-extra gv pgf dvipsk-ja git build-essential


